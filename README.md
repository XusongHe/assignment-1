## Assignment 1: Introduction to C++

The purpose of this assignment is to practice writing basic C++ code.

## General Implementation Constraints

Cite any ideas or bits of codes that you use, modify, or take inspiration
from. Just put a comment in your source code to the website or resource that
you referred to. Not citing help in this, even if accidentally forget, is
considered academic dishonesty!

You **can** use helper functions **that you write** if you like.


## Question 1: A Program Generator

Write a program that reads in the user's name, and also the name of a .cpp
file. The program then prints to `cout` another C++ program that, when run,
prints "Hello {name}!", where {name} is the name the user typed in. The file
name the user types in appears in a comment for the generated program as shown
below.

Here's a sample run:
```
What's your name? John
What's the name of the output file? gen

// gen.cpp

#include <iostream>

int main() {
    std::cout << "Hello John!\n";
}
```

The generated program printed to the screen should be formatted as in this
example, and should:

- Put the file name typed by the user in a comment at the top. Importantly,
  your program should add ".cpp" to the end of the file name typed by the
  user. If the file name already ends with ".cpp", then don't change the file
  name.

- Use the same spacing and indentation that is shown.

- Print the name typed by the user.

Here's another sample run. Notice that this time the file name typed by the
user ends with ".cpp":

```
What's your name? Mary
What's the name of the output file? hello.cpp

// hello.cpp

#include <iostream>

int main() {
    std::cout << "Hello Mary!\n";
}
```

Put your program into [hello.cpp](hello.cpp). The marker will compile it with
the [makefile](makefile) for this assignment using the command `make hello`.
The marker will also check that the generated program printed compiles and
runs correctly.


## Question 2: Numeric Type Limits
Write a program that prints a table of the numeric limits for the following
data types:

- `int`: # of bits, min, max

- `unsigned int`: # of bits, min, max

- `long`: # of bits, min, max

- `long long`: # of bits, min, max

- `unsigned char`: # of bits, min, max; make sure to print these values as
  integers, not characters

- `float`: # of bits, min, max, epsilon

- `double`: # of bits, min, max, epsilon

- `char`: # of bits, min, max

To calculate the number of bits in a type `T`, use the formula `sizeof(T)*8`.
For example, an `int` has `sizeof(T)*8` bits.

To get the min, max, and epsilon values for a type `T`, put `#include
<limits>` at the top of your program and use `numeric_limits<T>::min()`,
`numeric_limits<double>::max()`, and `numeric_limits<T>::epsilon()`. Epsilon,
also known as *machine epsilon*, is useful for floating point types and is the
smallest positive value that type can represent.

**Important**: You *must* use `numeric_limits` because the number of bits in a
type depends upon the computer and compiler the program runs on. If you just
print the numbers, they could be wrong for some computers.

Neatness counts! Make the output as neat and as easy to read as possible.

Put your program into [numeric.cpp](numeric.cpp). The marker will compile it
with the [makefile](makefile) for this assignment using the command `make
numeric`.


## Question 3: Password Checking
Write a program that asks the user to enter a new valid password. A password
is valid if **all** of these things are true:

- it's at least 8 characters long

- it has **no whitespace characters**; for this question whitespace characters
  are `' '`, `'\t'`, `'\n'`, and `'\r'`

- at least 1 character is a digit, 0-9

- at least 1 character is a lowercase letter, a-z

- at least 1 character is an uppercase letter, A-Z

- at least 1 character is one of these four punctuation characters: `!@#$`

Here are some examples:

- "Elm6Tree!", valid

- "ElmTree", invalid (no digits or punctuation)

- "Em6Tre!", invalid (too short)

- "Elm6 Tree!", invalid (contains a space)

If the password entered by the user is invalid, then your program should ask
the user to try again and enter another password. It keeps re-asking the user
until they enter a valid password.

If the password is valid, your program should ask the user to re-enter the
same password. If the user enters the same password, the program prints
"password successfully set", and ends. If the user enters a different
password, the program prints "error: password mismatch", and ends.

For simplicity, you can read the passwords into a `string` using `cin`.

Put your program for this question into [password.cpp](password.cpp). The
marker will compile your program with the [makefile](makefile) for this
assignment using the command `make password`.


## Question 4: Rotating Red Square

Using SFML, draw a red-filled square of side length 300 in the middle of a 500
by 500 window. The center of the square should be on the center of the window.

Make the square rotate around its center point at a rate of 1 to 4 degrees per
iteration of the main loop. The rate of rotation depends upon the X-position
of the mouse pointer, which we'll call mouseX. If mouseX is 0 or less, then
square's rotation rate is 1. If mouseX is 499 or more, then it's rotation rate
is 4. If 0 < mouseX < 499, then the rate of rotation should be proportional to
the value of mouseX. For example, if mouseX is about 250 (halfway between 0
and 499), then the rotation rate would be 2. If mouseX was 125, then the
rotation rate would be 1. As the mouseX position changes, the rate of rotation
should change proportionally.

Also, when the user clicks the left mouse button, the direction of the
rotation should change, i.e. from clockwise to counter-clockwise, or
counter-clockwise to clockwise. Clicking the left mouse doesn't change the
rate of the rotation.

![A red square](red_square.png)

Put your program for this question into [red_square.cpp](red_square.cpp). The
marker will compile your program with the [makefile](makefile) for this
assignment using the command `make red_square`.


## Question 5: A Drawing Language

Write an SFML program that reads a text file where each line of the file has a
command that draws a square or circle on a 500 by 500 window. For example,
[picture1.txt](picture1.txt) has these two lines:

```
red rectangle 100 125 250 250
yellow circle 38 250 245
```

The first line draws a red-filled rectangle of width 100 and height 250 with
its center point at location (250, 250) in the window. On top of that, the
second line draws a yellow-filled circle of radius 38 with its center point at
x-y location (250, 245) in the window.

![Yellow circle on a red rectangle](picture1.png)

Every line of a valid text file has one of these formats:

- [c] rectangle [w] [h] [x] [y]; means to draw a c-filled rectangle with width
  w, height h, and whose center point is at location (x, y) in the window.

- [c] circle [r] [x] [y]; means to draw a c-filled circle with radius r, and
  whose center point is at location (x, y) in the window.

The allowable color names are: black, white, red, green, blue, yellow,
magenta, and cyan.

For simplicity, you can assume that every line of the text file is properly
formatted.

To read the text file, use the terminal `<` operator:

```bash
$ ./draw < picture1.txt
```

When you use the `<` operator like this, your program only needs to read from
`cin`. The program will act as if the user had typed in everything that
appears in [picture1.txt](picture1.txt).

You can use `<` with any file. For example, [target.txt](target.txt) draws
concentric black and white circles:

```bash
$ ./draw < target.txt
```

![Black and white target pattern](target.png)

You are given [draw.cpp](draw.cpp) as a starting program. **Don't make any
changes to [draw.cpp](draw.cpp)!** All your code goes in [draw.h](draw.h). You
may only include standard C++ files, and all the code in [draw.h](draw.h)
should be written by you.

Put your code in [draw.h](draw.h). The marker will put this in the same folder
as their copy of [draw.cpp](draw.cpp), and then compile it with the
[makefile](makefile) for this assignment using the command `make draw`.


## Submitting Your Work

Put all the files requested by the questions into a .zip archive named
`a1_submit.zip`, and submit it on [Canvas](https://canvas.sfu.ca/).

The easiest way to create a .zip file for this assignment is to use the 
[makefile](makefile) command `make submit`:

```bash
$ make submit
rm -f 1_submit.zip
zip a1_submit.zip hello.cpp password.cpp numeric.cpp draw.h
...
```

## Basic Requirements

Before we give your programs any marks, these basic requirements must be met:

- Your programs must compile on Ubuntu Linux using the [makefile](makefile)
  for this assignment. If your program for a question doesn't compile, you
  will get 0 for that question.

  Please note that if you choose to develop your programs on a compiler other
  than g++ or without using the given [makefile](makefile), you are still
  responsible for ensuring it compiles and runs properly with g++ and the
  given [makefile](makefile).

- The [statement of originality](originality_statement.txt) has all your
  requested student information.

  You only need to modify and include this file once for the whole assignment.

  If this is incomplete or not included, we will assume your work is not
  original, it will not be marked, and you will get 0 for this assignment.

If your program meets all these basic requirements, then it will be graded
using the marking scheme below.


## Marking Scheme

### Source Code Readability (6 marks)

- All names of variables, functions, structs, classes, etc. are sensible,
  self-descriptive, and consistent.

- Indentation and spacing is perfectly consistent and matches the structure of
  the program. All blank lines and indents have a reason.

- All lines are 100 characters in length, or less.

- Comments are used when appropriate, e.g to describe code that is tricky or
  very important. There are no unnecessary comments, and no commented-out
  code.

- Appropriate features of C++ are used in an appropriate way. For example, do
  **not** use a feature of C (like C-style strings) when there is a better C++
  feature (e.g. the standard `string` class). Don't use any features you don't
  understand.

- Overall, the source code is easy to read and understand.

### A Program Generator (7 marks)

- (1 mark) Correctly asks the user for their name, and the name of a file

- (1 mark) Adds ".cpp" to any file name that doesn't already end with ".cpp".

- (1 mark) Generated program has good indentation and spacing.

- (1 mark) The correct name of the file appears in a comment at the top of the
  generated program.

- (1 mark) The name typed by the user appears correctly in the generated
  program.

- (2 marks) The generated program compiles and runs correctly.


### Numeric Type Limits (5 marks)

- (4 marks) Correctly displaying the requested information for each data type
  is worth 0.5 marks.

- (1 mark) Program output is beautifully formatted.


### Password Checking (5 marks)

- (3 marks) Each of the six constraints for what makes a valid password is
  worth 0.5 marks for a correct implementation

- (1 mark) The program correctly re-asks the user to enter a password when
  they don't enter a valid password.

- (1 mark) After the user enters a valid password, the program asks the user
  to re-enter it, and prints the appropriate message if its the
  same/different.

### Rotating Red Square (5 marks)

- (1 mark) Drawing a red-filled square in the center of the screen. The
  dimensions of the square and the window are as given in the question.

- (1 mark) The square rotates around its center point.

- (1 mark) The speed of the rotation of the square is proportional to the
  x-value of the location of the mouse pointer.

- (1 mark) The rotation speed is constrained so that if the mouse-pointer is
  to the left of the window, then the rotation is the same as if the mouse at
  the left-most side of the window. Similarly, if the the mouse-pointer is to
  the right of the window, then the rotation is the same as if the mouse at
  the right-most side of the window.

- (1 mark) When the user clicks the left mouse button, the direction of the
  rotation changes (without changing the speed).

### A Drawing Language (6 marks)

- (2 marks) Rectangles are correctly drawn.

- (2 marks) Circles are correctly drawn.

- (2 marks) The input is correctly processed.

### Following Submission Instructions (1 mark)

- (1 mark) The submitted `.zip` file has the proper name, and contains exactly
  the requested files.
